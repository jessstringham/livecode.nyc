---
title: Tools
---
## Made around the world

These are tools made by people we admire that take part in the global live-coding community. 

<br/>

### Sonic PI (Ruby) [Website](https://sonic-pi.net/)

Sonic Pi is a tool to create music using Ruby.

### TidalCycles (Haskell) [Website](https://tidalcycles.org/index.php/Welcome)

"TidalCycles is a live coding environment designed for musical improvisation and composition. In particular, it is a domain-specific language embedded in Haskell, focused on the generation and manipulation of audible or visual patterns." [(Wikipedia)](https://en.m.wikipedia.org/wiki/TidalCycles)

### FoxDot (Python) [Website](https://foxdot.org/)

Live-coding music using Python and SuperCollider. 

### Orca [Website](https://github.com/hundredrabbits/Orca)

"Orca is an esoteric programming language, designed to create procedural sequencers in which each letter of the alphabet is an operation, where lowercase letters operate on bang, uppercase letters operate each frame." [(GitHub)](https://github.com/hundredrabbits/Orca)

<br/>

## For Visuals:

<br/>

### Hydra (JavaScript) [Website](https://github.com/ojack/hydra) [Editor](https://hydra-editor.glitch.me)

Hydra is a "set of tools for livecoding networked visuals. Inspired by analog modular synthesizers, these tools are an exploration into using streaming over the web for routing video sources and outputs in realtime." [(GitHub)](https://github.com/ojack/hydra)

### Live Code Lab (CoffeeScript) [Website](https://livecodelab.net/) [Editor](https://livecodelab.net/play/index.html)

"Livecodelab is a special secret place where you can make fancy "on-the-fly" 3d visuals and play awesomely offbeat (literally) sounds. "On-the-fly" meaning: as you type. Type just three letters: "box", and boom! a box appears. No clicking play, no waiting, no nothing." [(Website)](https://livecodelab.net/)

### Visor (Ruby) [Website](http://www.visor.live/)

"Visor is a live coding environment for real-time visual performance. Visor bridges the gap between creative coding and VJing by offering user interfaces to easily interact with live coded Processing sketches." [(Website)](http://www.visor.live/)

### P5LIVE (JavaScript) [Website] (https://teddavis.org/p5live/)

"p5.js collaborative live-coding vj environment!"