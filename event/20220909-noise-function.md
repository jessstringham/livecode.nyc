---
title: Noise Function
location: littlefield, 635 Sackett St, Brooklyn, NY 11217
date: 2022-09-09 8:00 PM
duration: 4:00
image: /image/2022-noise-function.jpg
link: https://www.eventbrite.com/e/noise-function-a-livecodenyc-showcase-tickets-396694793807
categories: [algorave, music, art, noise, computers]
---

# Noise/Function
You are cordially invited to our noise function. 
A night of experimental live coded music and art on with a noisy bent.

