---
title: Livecode Meetup!
location: Hex House, 366 Devoe Street, Brooklyn, NY 11211
date: 2022-10-23 1:30 PM
duration: 3:00
image: /media/logo-white.svg 
categories: [meetup, code, library, music, art, computers]
---

# NEXT MEETING
Join us for one of our regular meetups!
This is a low-key event to gather, discuss and connect with livecoders. Members are welcome to come in and out as the please.
Bring a laptop and cowork, ask for advice, or wax philiosophical about computers.

