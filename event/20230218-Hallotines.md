---
title: Hallotines
location: Wonderville, 1186 Broadway, Brooklyn, NY 11221
date: 2023-02-18 8:00 PM
duration: 4:30
image: /image/2023-hallotines.png
categories: [algorave, music, art, computers, halloween, valentines]
link: https://withfriends.co/event/15642653/fear_and_loving_in_brooklyn
---

# Fear and Loving in Brooklyn:
### A show in which livecode.nyc will single handedly kill valentines day and replace it with halloween II

We reject cruel consumerism. We reject hallmark obligations. We choose instead to embrace shadowy expressiveness and joy in the arcane.
**Join us for a better holiday.**

Get ready for livecode.nyc's celebration of love and fear and code... We'll dance the night away to haunting and hard hitting melodies reminiscent of your third crush and the undeniable romance of horror movie dates. 

Come for the live code, stay for the costume contest! 

💀 Win one of three categories, Halloweeniest, Valentinest, and the Most Sickening! 

💝 Prizes are in the form of chocolate hearts.

🪱 Masks mandatory event.


### Lineup
[Shellylynnx](https://www.instagram.com/shellylynnx/) + [Shane Curry](https://www.instagram.com/chewgum_playgames/)

[Voyde](https://www.instagram.com/voydescreamsback/) + [Luciform](https://www.instagram.com/_luci.form/)

[gwenprime](https://gwenpri.me/) + [pemdaskapital](https://www.instagram.com/pemdaskapital/)

[snowangel](https://www.instagram.com/s_n_o__w__/) + [kid reno](https://www.instagram.com/selfiejesus/)

[Permian Strata](https://www.instagram.com/permianstrata/) + [Dog Collar](https://www.instagram.com/dogxcollar/)
