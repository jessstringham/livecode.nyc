---
title: Pixel Worship
location: 337 Butler St, Brooklyn, NY 11217
date: 2022-11-19 8:00 PM
duration: 4:30
image: /image/2022-pixel-worship.png
categories: [algorave, music, art, computers]
---

# Pixel Worship
### A night to cherish our screens.

Brought to you by LivecodeNYC
Masks, please.

### PERFORMANCES BY:
- Andy Rolfes
- DJ_DAVE
- The Poet Engineer
- Luciform
- easterner
- alsoknownasrox
- d0n.xyz
- Dog Collar
- Lineup TBA

