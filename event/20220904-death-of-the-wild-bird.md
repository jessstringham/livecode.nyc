---
title: Death of the Wild Bird
location: Wild Birds, 951 Dean St. Brooklyn, NY 11238
date: 2022-09-04 4:00 PM
duration: 2:00
image: /media/logo-white.svg
categories: [algorave, music, art, computers]
---
First and last audio-visual show at the beloved Brooklyn live-music venue Wild Birds before its imminent closure September 5th. Within the themes of gathering, mourning, healing, rituals, and celebration.


