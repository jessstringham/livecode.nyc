---
title: "MY _____ IS AN ECOSYSTEM Day 3"
location: Harvestworks Art and Technology Program, Building 10a, Nolan Park, Governors Island
date: 2024-04-28 1:00 PM
duration: 4:00
link: https://www.harvestworks.org/livecodenyc2024-apr-26-may-5/ 
image: /image/govisland2024_insta_main.png
categories: [algorave, music, art, computers]
---

1pm – Workshop: Reconcile of Sounds & Found in Translation

A workshop about constructing sounds from different Southeast Asian tuning and sound cultures using Orca, a live coding language for sequencing, and other open-source tools by elekhlekha อีเหละเขละขละ.


3pm – Audiovisual performances by Saddle Up the Robots, the code, and Archaic Reckoner


A workshop about constructing sounds from different Southeast Asian tuning and sound cultures using Orca, a live coding language for sequencing, and other open-source tools by elekhlekha อีเหละเขละขละ.

Processes. Loops. Glitch. Noise. Feedback. Nostalgia. Sustainability. World building. An ecosystem of ecosystems. This year’s show, My ______ is an Ecosystem (said “my blank is an ecosystem”) features a selection of art, workshops, discussions, and performances that explore the former ideas in many ways.

From interactive installations to video art, and audiovisual performances to sculpture, this exhibition features artists using live coding, artificial intelligence, custom electronics and software, repurposed electronics and e-waste, permacomputing, and more. 

The title of the show refers to the relationships these featured artists build with their imaginations and tools. An alternative name for this show could be “This is an ecosystem,” referring to this collection of work, the event, and the community of artists involved.
