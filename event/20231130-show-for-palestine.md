---
title: Code of Resistance
location: Gold Sounds Bar, 44 Wilson Ave, Brooklyn, NY 11237
date: 2023-11-30 8:00 PM
duration: 3:00
image: '/image/codeofresistance.png'
link: https://dice.fm/partner/44-wilson-ave-llc/event/wpk9v-code-of-resistance-30th-nov-gold-sounds-new-york-tickets
categories: [algorave, music, art, computers]
feed:
    enable: true
---

# Benefit Shoiw for Palestinian Action

Join us next week for an extraordinary show dedicated to supporting Palestine's media groups and journalists. Get ready for an evening filled with live coded music and visuals and inspiring readings.


Let's come together for a cause that matters. See you there!


Featuring:

carnal ex

snow

v10101a + aerosoul

hexe.exe + emptyflash_

Azhad Syed + Genesis rogue
