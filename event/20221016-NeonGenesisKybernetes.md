---
title: Neon Genesis Kybernetes
location: Visaural, 47 thames street, Brooklyn NY 11237
date: 2022-10-16 7:00 PM
duration: 4:00
image: /media/logo-white.svg
categories: [algorave, music, art, computers]
---

15 years after the Second Impact and the agents of LVCD are creating art and music with code. 
Come dance with us.

