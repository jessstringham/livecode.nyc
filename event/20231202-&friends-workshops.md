---
title: '& Friends Workshops'
location: Brooklyn Art Haus
date: 2023-12-02 3:00 PM
duration: 2:00
link: https://shotgun.live/events/friends-workshops
image: '/image/&friends_workshops.avif'
categories: [algorave, music, art, computers]
feed:
    enable: true
---

Join us for a series of workshops on creative code art processes and programming on Dec, 2 during our & Friends Event! This ticket will grant you access to all 3 workshops and our artist talk featuring AlbertDATA on tour from Barcelona. 


Brought to you by Creative Code Art, livecodenyc & AlbertDATA


Note: We have a no turn away policy for those that can't afford to purchase a ticket. Please email: 
creativecodeart@gmail.com
 to request a spot.  Tickets for the live a/v performances are sold separately. 


## Session Details: 
3pm: WORKSHOP 1:  Streamlining Workflows for Artistic Expression (Touchdesigner + TDAbleton + Unreal Engine) with the Glad Scientist. 


The Glad Scientist demystifies the integration of TouchDesigner, TDAbleton, and Unreal Engine, offering insights and techniques to enhance your artistic process. Whether you're a seasoned creator or a curious beginner, this workshop promises to unlock new dimensions of artistic expression and efficiency in your workflow. Don't miss the chance to learn from the Glad Scientist's expertise and elevate your creative endeavors to new heights.



3pm: WORKSHOP 2: Coding Music With Strudel with Dan Gorelick & Viola He


In the workshop Dan and Viola covers: Making your first sound (the "Hello World"), Mini notation, Notes, Samples, Synths, Scales, Effects, Euclidean Rhythms, and advanced topics such as MIDI, audio routing, custom samples, and Dan and Viola's performance setups.



4pm: WORKSHOP 3:  Bytebeats, Bitfield Patterns, and Permacomputing with Cameron


Cameron will give a workshop titled Bytebeats, Bitfield Patterns, and Permacomputing in which he will give an overview of the musical concept of bytebeats, their visual analog bitfield patterns, and how the two relate to resilient and regenerative computing.



4pm: ARTIST TALK: On disembodiment, extended cognition, hybrid beings & synthetic identities with AlbertDATA


Join us for a thought-provoking artist talk with the visionary AlbertDATA as we delve into the realms of disembodiment, extended cognition, and the creation of hybrid beings and synthetic identities. Explore the intersection of art and technology as AlbertDATA guides us through a captivating conversation that challenges traditional notions of existence and identity. This event promises to ignite your imagination and spark discussions on the evolving landscape of human experience in our increasingly digital and interconnected world. Don't miss the opportunity to engage with one of the most innovative minds at the forefront of contemporary art and technology.
