# Press

LiveCode.NYC:

- [Financial Times](https://www.ft.com/content/0b64ec04-c283-11e9-ae6e-a26d1d0455f4)
- [New York Times](https://www.nytimes.com/2019/10/04/style/live-code-music.html)
- [Vice](https://www.vice.com/en_us/article/j5wgp7/who-killed-the-american-demoscene-synchrony-demoparty)
- [Alternative Press](https://www.altpress.com/algorave-live-coding-scene-explained/)

<br/>

International Community:

- [Resident Advisor](https://www.residentadvisor.net/features/3396)
