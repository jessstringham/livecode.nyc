---
name: bl4st
link: https://emptyfla.sh/bl4st
image: /image/bl4st.png
---

Livecodable real-time fractal flames in the browser with JavaScript. [Flam3's](https://flam3.com/) are a type of Iterated Function System that
generate fractals that can look similar to flames. bl4st is made by [Cameron Alexander](/member/cameron.html).
