---
name: Kate Sicchio
image: http://blog.sicchio.com/wp-content/uploads/2013/09/IMG_20140908_172439.jpg
links:
    website: http://blog.sicchio.com
---

Live coding choreography and dance through all means possible. One of the sound artists in Codie. Teaches a lot of Sonic Pi.
